﻿<html>
<head>
    <meta charset="utf-8">
    <title>WebSocket Logger</title>
    <script src="/api/js/jquery-3.2.1.min.js"></script>
    <#--stomp协议使用-->
    <script src="/api/js/sockjs.min.js"></script>
    <script src="/api/js/stomp.min.js"></script>
</head>
<body>
<h1>jvm进程内的日志</h1>
<button onclick="clearLog()">清理日志</button><button onclick="closeSocket()">断开连接</button>
<div id="log-container" style="height: 900px; overflow-y: scroll; background: #333; color: #171b45; padding: 10px;">
    <div></div>
</div>

</body>
<script language="javascript" type="text/javascript">

    var wsUri ="ws://172.18.134.32:8080/api/connect/"+123;
    websocket = new WebSocket(wsUri);
    websocket.onopen = function(evt) {
        console.log('链接开通····');
        console.log(evt)
    };
    websocket.onmessage = function(evt) {
        console.log('`````接收数据中`````');

        // websocket.send("Hello server!");
        var theData = JSON.parse(evt.data);
        //日志
        if(theData.type == 'log'){
            var content = JSON.parse(theData.data);
            console.log(content)
            if(content.level == "DEBUG"){
                $("#log-container div").append("<sapn style='color:blue;'>"+content.timestamp +" "+ content.level+" --- ["+ content.threadName+"] "+ content.className+"   :"+content.body).append("<br/>"+"</sapn>");

            }else if(content.level == "INFO"){
                $("#log-container div").append("<sapn style='color:#44ff24;'>"+content.timestamp +" "+ content.level+" --- ["+ content.threadName+"] "+ content.className+"   :"+content.body).append("<br/>"+"</sapn>");

            }else if(content.level == "WARN"){
                $("#log-container div").append("<sapn style='color:orange;'>"+content.timestamp +" "+ content.level+" --- ["+ content.threadName+"] "+ content.className+"   :"+content.body).append("<br/>"+"</sapn>");

            }else if(content.level == "ERROR"){
                $("#log-container div").append("<sapn style='color:red;'>"+content.timestamp +" "+ content.level+" --- ["+ content.threadName+"] "+ content.className+"   :"+content.body).append("<br/>"+"</sapn>");
            }else{
                $("#log-container div").append("<sapn style='color:green;'>"+content.timestamp +" "+ content.level+" --- ["+ content.threadName+"] "+ content.className+"   :"+content.body).append("<br/>"+"</sapn>");
            }
            $("#log-container").scrollTop($("#log-container div").height() - $("#log-container").height());
        }

    };
    websocket.onerror = function(evt) {
        console.error('链接错误····');
        console.log(evt)
    };
    websocket.onclose = function(evt) {
        console.log('链接关闭····');
        console.log(evt)
    };

    //清理日志
    function clearLog() {
        $("#log-container div").empty();
    }
    //断开连接
    function closeSocket() {
         websocket.close();
    }
</script>
</body>
</html>