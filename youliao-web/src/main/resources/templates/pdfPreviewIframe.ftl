<html>
<head>
    <script src="/api/js/jquery-3.2.1.min.js"></script>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
    <meta http-equiv="Cache-Control" content="no-store"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="0"/>
    <title>PDF预览</title>
</head>
<body>
<script type="text/javascript">

    var file = "";
    $(document).ready(function () {

        //先将freemarker数据获取出来
       <#if file??>
          file = "${file}";
       </#if>
    });
    // 初始化内容
    $(function () {
        openScanFile();
    });

    function openScanFile() {
        var pdfUrl = "http://localhost:8080/api/generic/web/viewer.html?file=" + encodeURIComponent(file);
        var vm = window.open(pdfUrl);
    }

</script>
</body>
</html>
