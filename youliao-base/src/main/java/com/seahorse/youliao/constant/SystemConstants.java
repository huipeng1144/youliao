package com.seahorse.youliao.constant;

/**
 * @ProjectName: youliao
 * @Package: com.seahorse.youliao.constant
 * @ClassName: SystemConstants
 * @Description: 系统常量
 * @author:songqiang
 * @Date:2020-11-30 15:50
 **/
public class SystemConstants {

    /**
     * 系统管理员
     */
    private static final String ADMIN_USER = "admin";

    /**
     * 注册分配的角色名
     */
    private static final String REGISTER_ROLE_CODE = "ROLE_REGISTER";

    /**
     * 管理员角色名
     */
    private static final String ADMIN_ROLE_CODE = "ROLE_ADMIN";


}
